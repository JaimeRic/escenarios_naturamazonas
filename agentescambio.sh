#!/bin/bash

###   Elaborado por Jaime R. García Márquez  como parte del proyecto de consultoría "Escenarios para la governanza de los bosques en el piedemonte Andino-Amazónico"

### Procedimiento para la preparación de las variables incluidas en los análisis de escenarios


## ---------------------------------------------------------------
#################    Ajustes preliminares    ########################
## ---------------------------------------------------------------

###   declarar variable con el directorio de trabajo
export DIR=~/Nextcloud/naturamazonas/data

###   declarar variable con el directorio para archivar la versión final de las capas
export OUT=~/Nextcloud/naturamazonas/data/capas

###  declarar variable utilizada como máscara para el área de estudio
export MASKVECT=Mask_pol_utm_2.shp


## ---------------------------------------------------------------
#################    Variable RED VIAL    ########################
## ---------------------------------------------------------------

##   descargar de la fuente oficial en internet
wget https://opendata.arcgis.com/datasets/84b9561195b043a8b68bd1ca08af1fbf_0.geojson $DIR

##  cambiar de nombre
mv $DIR/84b9561195b043a8b68bd1ca08af1fbf_0.geojson $DIR/redvial.geojson

## convertir del formato original (geojson) al formato 'shapefile'; aplicar la transformación cartográfica a las proyección local; definir una ventana amplia alrededor de la zona de estudio para la extracción de las vias primarias y secundarias
ogr2ogr -f "ESRI Shapefile" -t_srs  EPSG:32618 -clipdst 154266 26488 505954 324157 $DIR/vias.shp $DIR/redvial.geojson

## Transformar del formato vector al formato raster
gdal_rasterize -a objectid -at -tr 30 30 -l vias $DIR/vias.shp $DIR/vias.tif -co COMPRESS=DEFLATE -co ZLEVEL=9

## calcular las distancias a las vias: los valores finales de cada pixel está dado en número de pixeles (distancia) a las vias.
gdal_proximity.py $DIR/vias.tif $DIR/vias_dist.tif -ot UInt16 -co COMPRESS=DEFLATE -co ZLEVEL=9 -nodata 9999

## creación de la capa de distancia a las vias para el área de estudio
gdalwarp $DIR/vias_dist.tif $OUT/vias_d_PIED.tif -co COMPRESS=DEFLATE -co ZLEVEL=9 -te 272007 76013 400721.8  193415 -dstnodata 9999 -cutline $MASKVECT -crop_to_cutline -overwrite


## ---------------------------------------------------------------
#################    Variable TOPOGRAFIA    ########################
## ---------------------------------------------------------------

### moverse para trabajar desde el directorio de datos
cd $DIR

###  descargar las dos ventanas del DEM que cubren a Colombia; correr de forma paralela usando dos cores del computador para ajustar la extensión de las ventanas y generar una nueva imagen tif para cada una;
for file in elv_s30w090.tar elv_00w090.tar  ; do

wget --user=hydrography  --password=rivernetwork http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v0.7/$file
tar -xf $file

export file
export filename=$(basename $file  .tar  )

ls $filename/*_elv.tif | xargs -n 1 -P 2  bash -c $'
tifname=$( basename $1 )

ulx=$(gdalinfo $filename/$tifname | grep "Upper Left" | awk \'{ gsub ("[(),]"," ") ; print  $3  }\' | awk \'{ printf ("%.0f ", $1)}\')
uly=$(gdalinfo $filename/$tifname | grep "Upper Left" | awk \'{ gsub ("[(),]"," ") ; print  $4  }\' | awk \'{ printf ("%.0f ", $1)}\')
lrx=$(gdalinfo $filename/$tifname | grep "Lower Right" | awk \'{ gsub ("[(),]"," ") ; print $3  }\' | awk \'{ printf ("%.0f ", $1)}\')
lry=$(gdalinfo $filename/$tifname | grep "Lower Right" | awk \'{ gsub ("[(),]"," ") ; print $4  }\' | awk \'{ printf ("%.0f ", $1)}\')

gdal_translate -a_nodata -9999 -co COMPRESS=DEFLATE -co ZLEVEL=9 -a_ullr ${ulx} ${uly} ${lrx} ${lry} $filename/$tifname $tifname
' _

### remover archivos temporales
rm -f  $file
rm -R  $filename

done

### crear archivo virtual uniendo las dos ventanas y luego convertir a un solo archivo TIF
gdalbuildvrt   -overwrite  -srcnodata -9999 -vrtnodata -9999    elv_all_tif.vrt *_elv.tif
gdal_translate -co COMPRESS=DEFLATE -co ZLEVEL=9 elv_all_tif.vrt elevacion.tif

### remover archivos temporales
rm -fr  *_elv.tif elv_all_tif.vrt

### creación de la capa de elevación para el área de estudio; ajustando la proyección, la resolución (resampling nearest neighbour para no modificar os valores de la resolución original ~90 m) y cortando al polígono del área de estudio
gdalwarp elevacion.tif $OUT/elevacion_PIED.tif  -t_srs EPSG:32618 -tr 30 30 -co COMPRESS=DEFLATE -co ZLEVEL=9 -te 272007 76013 400721.8  193415 -cutline $MASKVECT -crop_to_cutline -overwrite


## ---------------------------------------------------------------
#################    Variable HETEROGENEIDAD ELEVACIÓN  ##########
## ---------------------------------------------------------------

## como insumo (-i) se usa la capa de elevación creada anteriormente
## la heterogeneidad se calculó mediante la desviación estandar de los valores de elevación en una ventana de 50 x 450 metros
pkfilter -co COMPRESS=DEFLATE -co ZLEVEL=9 -nodata -9999 -i $OUT/elevacion_PIED.tif -o $OUT/elev_heterogeneidad_PIED.tif -dx 15 -dy 15 -f stdev
